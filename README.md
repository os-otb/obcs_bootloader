## Folder structure
```bash
.
└── Boot
    ├── App
    ├── FDIR
    ├── Net
    ├── System
    └── obcs_api_cfg
```

### Build

To build the project you must to pass the BOARD argument to make.

To compile OTB project:
```
make BOARD=OTB
```

To compile OpenSpace OBC project:
```
make BOARD=OS
```

### Develop in STM32CubeIDE
In the IDE project, we can find two build configuration:
* One for the OTB, this will define a build var BOARD=OTB
* One for the Open Space OBC, this will define a build var BOARD=OS_OBC

This build var will be used in the build command by the IDE.

### Misc
#### Config static IP address on ethernet port in Ubuntu 18.04

This is done with a tool called Netplan (it is on the distro already). The file to configure the ip address is located, generally, in /etc/netplan.
Must to assign an static ip address on the desired ethernet port. For example:

```yaml
# Let NetworkManager manage all devices on this system
# In this configuration, the eth0 port have an static ip address.
network:
  version: 2
  renderer: NetworkManager
  ethernets:
    eth0:
      dhcp4: no
      dhcp6: no
      addresses: [192.168.1.100/24]
      gateway4: 192.168.1.1
      nameservers:
        addresses: [8.8.8.8,8.8.4.4]
```

Then, must to apply the changes with:
```
sudo netplan apply
```

To be sure, confirm that the ethernet interface has the desired IP with ifconfig.

Source: https://www.howtoforge.com/linux-basics-set-a-static-ip-on-ubuntu
