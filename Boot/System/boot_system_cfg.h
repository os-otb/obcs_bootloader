/*
 * boot_system_cfg.h
 *
 *  Created on: May 1, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef BOOT_SYSTEM_CFG_H_
#define BOOT_SYSTEM_CFG_H_

// Include --------------------------------------------------------------------
#include "boot_system.h"
/* Tasks modules includes. */
#include "boot_app.h"
#include "boot_net.h"
#include "boot_fdir.h"
#include "boot_serial_osotb.h"

#include "api.h"	/* !< OBCs API include. */

// Macros & constants  --------------------------------------------------------

// Public variables declaration  ----------------------------------------------
typedef struct
{
  TaskHandle_t (*init_handler)(void *arg);
  void *init_argument;		/*!< Argument passed to initializing function. */
}boot_system_handler_t;

// Public variables definition  -----------------------------------------------
static const boot_system_handler_t boot_system_handler_table[] =
{
  {.init_handler = API_Logger_Init, .init_argument = NULL},			/*!< BOOT_SYSTEM_TASK_LOGGER. */
  {.init_handler = boot_serial_osotb_init, .init_argument = NULL},
  {.init_handler = boot_fdir_init, .init_argument = NULL},			/*!< BOOT_SYSTEM_TASK_FDIR. */
  {.init_handler = boot_app_init, .init_argument = NULL},			/*!< BOOT_SYSTEM_TASK_APP. */
  {.init_handler = boot_net_init, .init_argument = NULL},			/*!< BOOT_SYSTEM_TASK_NET. */
};

#define BOOT_SYSTEM_SIZEOF_TASK_TABLE (sizeof(boot_system_handler_table) / sizeof(boot_system_handler_table[0]))
#endif /* SYSTEM_INC_BOOT_SYSTEM_CFG_H_ */
