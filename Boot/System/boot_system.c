/*
 * boot_system.c
 *
 *  Created on: May 1, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "boot_system_cfg.h" 	/* !< System module private include. */

// Macros & constants ---------------------------------------------------------
#define BOOT_SYSTEM_LOG_ENABLED			1	/*<! Enables module log. */

#define	LOG_TAG							"boot_sys"	/*<! Tag assigned to logs for this module. */

#if BOOT_SYSTEM_LOG_ENABLED
	#define BOOT_SYSTEM_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_SYSTEM_LOG_WARNING(format, ...)		LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_SYSTEM_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_SYSTEM_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define BOOT_SYSTEM_LOG_ERROR(...)
	#define BOOT_SYSTEM_LOG_WARNING(...)
	#define BOOT_SYSTEM_LOG_INFO(...)
	#define BOOT_SYSTEM_LOG_DEBUG(...)
#endif

// Private variables declaration ----------------------------------------------
typedef struct
{
	struct
	{
		uint32_t created;						/*<! Number of tasks created. */
		TaskHandle_t handles[BOOT_SYSTEM_SIZEOF_TASK_TABLE];	/*<! Handles of the tasks created. */
	}tasks;
} boot_sys_t;

// Private variables definition -----------------------------------------------
static boot_sys_t boot_sys;

// Private functions declaration ----------------------------------------------
ret_code_t boot_system_init(void);
bool boot_system_check_boot_condition(void);
void boot_system_force_reset(void);
ret_code_t boot_system_init_api(void);

// Public functions definition  -----------------------------------------------
/**
 * @brief  The entry point of the application.
 * @retval int
 */
int main(void)
{
	ret_code_t ret = RET_OK;

	/* Init boot_sys control struct. */
	memset(&boot_sys, 0, sizeof(boot_sys));

	ret = boot_system_init_api();
	if(ret == RET_OK)
	{
		if(true == true/*boot_system_check_boot_condition()*/)
		{	/* The condition for Boot process is true, so run the Boot code. */
			boot_system_init();
			/* @note never reach this point. The boot_system_init() function
			 * must to launch the scheduler. */
		}
		else
		{	/* The condition for Boot process is not true, so run the user application code.
		 	 * First, we need to get the default app to run. This is burned in the flash of the MCU. If there
		 	 * is not any app slot in the flash then program will boot from one defined by the user in the
		 	 * configuration. */
			api_iap_app_slot_t def_slot = api_iap_get_default_app();
			if( true == api_iap_app_has_good_integrity(def_slot) )
			{	/* If the default slot has good integrity, jump to the default. */
				api_iap_jump_to_app(def_slot);
			}
			else
			{	/* If the default slot doesn't have good integrity, jump to the another slot. */
				api_iap_app_slot_t other_slot = (API_IAP_APP0_SLOT == def_slot) ? API_IAP_APP1_SLOT : API_IAP_APP0_SLOT;
				api_iap_jump_to_app(other_slot);
			}
		}
	}
	else
	{
		/* TODO handle this error. */
	}

	/* We should never get here as control is now taken by the scheduler */
	while (1)
	{

	}
}

// Private functions definition  ----------------------------------------------
/*
 * @brief init Boot code.
 * */
ret_code_t boot_system_init(void)
{
	ret_code_t ret = RET_OK;

	for(int c = 0; c < BOOT_SYSTEM_SIZEOF_TASK_TABLE ; c++)
	{
		if(NULL != boot_system_handler_table[c].init_handler)
		{
		  boot_sys.tasks.handles[boot_sys.tasks.created] = boot_system_handler_table[boot_sys.tasks.created].init_handler(boot_system_handler_table[boot_sys.tasks.created].init_argument);
		  boot_sys.tasks.created++;
		}
	}

	/* Start scheduler */
	vTaskStartScheduler();
	/* NOTE never must to reach this point. */
	return ret;
}

/*
 * @brief check the Boot condition.
 * @return true if the system must to make Boot or false otherwise.
 * */
bool boot_system_check_boot_condition(void)
{
	ret_code_t ret = RET_OK;
	bool cond = true;

	ret = api_iap_check_bootloader_condition(&cond);
	if( RET_OK != ret )
	{
		/* TODO What we do here? */
	}

	return cond;
}

/*
 * @brief forces a system reset.
 * */
void boot_system_force_reset(void)
{
//	NVIC_SystemReset();
}

/**
 * @brief initialize the APIs needed by the Boot process.
 **/
ret_code_t boot_system_init_api(void)
{
	ret_code_t ret = RET_OK;

	/* When the User Code inits the API, the API inits the Board. */
	ret = API_Board_Init();
	if( RET_OK != ret )
	{
		/* TODO Handle error. */
	}

	/* Datetime must be initialized before all the APIs because
	 * its used for them. */
	ret = API_Datetime_Init();
	if( RET_OK != ret )
	{
		/* TODO Handle error. */
	}

	ret = API_FS_Init();
	if( RET_OK != ret )
	{
		/* TODO Handle error. */
	}

	/**
	 * TODO:
	 * - Init FLASH API (verifies that the flash is unprotected).
	 **/
	return ret;
}
