/*
 * boot_fdir_private.h
 *
 *  Created on: Aug 17, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef BOOT_FDIR_PRIVATE_H_
#define BOOT_FDIR_PRIVATE_H_

// Includes -------------------------------------------------------------------
/* Tasks modules includes. */
#include "boot_fdir.h"

#include "api.h"	/* !< OBCs API include. */

#endif /* FDIR_BOOT_FDIR_PRIVATE_H_ */
