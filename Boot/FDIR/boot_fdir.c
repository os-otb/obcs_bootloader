/*
 * boot_fdir.c
 *
 *  Created on: Aug 16, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include "boot_fdir_private.h"

// Macros & constants ---------------------------------------------------------
/* @brief system housekeeping task configuration parameters. */
#define BOOT_FDIR_TASK_STATUS_PERIOD 30000	/*<! Period (in ms) in which the system housekeeping analyzes tasks status. */
#define BOOT_FDIR_TASK_SANITY_LED_TOGGLE_PERIOD 2000 /*<! Period (in ms) of the sanity led toggling (debug purposes). */
#define BOOT_FDIR_TASK_FS_HEALTH_CHECK_PERIOD	120000 /*<! Period (in ms) of the sanity led toggling (debug purposes). */
#define BOOT_FDIR_TASK_FREE_HEAP_PERIOD 10000	/*<! Period (in ms) in which the fdir task checks the free heap on the mcu. */

#define BOOT_FDIR_TASK_STACK_SIZE				512
#define BOOT_FDIR_TASK_PRIOTY		osPriorityNormal

#define BOOT_FDIR_LOG_ENABLED			1	/*<! Enables module log. */

#define	LOG_TAG							"boot_fdir"	/*<! Tag assigned to logs for this module. */

#if BOOT_FDIR_LOG_ENABLED
	#define BOOT_FDIR_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_FDIR_LOG_WARNING(format, ...)		LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_FDIR_LOG_INFO(format, ...)			LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_FDIR_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define BOOT_FDIR_LOG_ERROR(...)
	#define BOOT_FDIR_LOG_WARNING(...)
	#define BOOT_FDIR_LOG_INFO(...)
	#define BOOT_FDIR_LOG_DEBUG(...)
#endif

#define BOOT_FDIR_CHECK_FILE_DIR	"/health_test" /*!< Directory where the health_test file is created. */
#define BOOT_FDIR_CHECK_FILE_NAME	"/health.txt" /*!< Name used for the test file on the Disk. */
#define BOOT_FDIR_CHECK_FILE_PATH	BOOT_FDIR_CHECK_FILE_DIR BOOT_FDIR_CHECK_FILE_NAME

// Private variables declaration ----------------------------------------------
typedef struct
{
	struct
	{
		TickType_t get_task_status_info;
		TickType_t sanity_led_toggle;
		TickType_t fs_check;
		TickType_t free_heap;
		TickType_t now;
	}ts;
	TaskHandle_t task_handle;
} boot_fdir_t;

// Private variables definition -----------------------------------------------
static boot_fdir_t boot_fdir;

// Private functions declaration ----------------------------------------------
static ret_code_t boot_fdir_check_fs(void);
void boot_fdir_task(void *arg);

// Public functions definition ------------------------------------------------
TaskHandle_t boot_fdir_init(void *arg)
{
	if( xTaskCreate(	boot_fdir_task, "boot_fdir",
						BOOT_FDIR_TASK_STACK_SIZE,
						NULL,
						BOOT_FDIR_TASK_PRIOTY,
						&boot_fdir.task_handle ) != pdPASS )
	{
		/* If the task can't be created the task handle will be NULL. */
		boot_fdir.task_handle = NULL;
	}

	return boot_fdir.task_handle;
}

// Private functions definition -----------------------------------------------
/*
 * @brief system housekeeping task.
 * */
void boot_fdir_task(void *arg)
{
	int now = 0;
	/* Make an array with all the tasks executing on the system. */
	size_t free_heap = 0;
	UBaseType_t uxArraySize = 0;
	TaskStatus_t *pxTaskStatusArray = NULL;
	uxArraySize = uxTaskGetNumberOfTasks();
	pxTaskStatusArray = pvPortMalloc(uxArraySize*sizeof(TaskStatus_t));
	if( NULL != pxTaskStatusArray ) uxTaskGetSystemState(pxTaskStatusArray, uxArraySize, NULL);

	while(true)
	{
		now = TICKS_TO_MS(xTaskGetTickCount());
		if( BOOT_FDIR_TASK_STATUS_PERIOD < now-boot_fdir.ts.get_task_status_info )
		{	/* Log the free stack of each task. */
			if( NULL != pxTaskStatusArray )
			{
				for( int x = 0; x < uxArraySize; x++ )
				{
					/* Don't include the state of the task in the query (takes too long and we don't
					 * need it). The high watermark takes too long too, but is an important parameter to track.
					 * Just mantain high the period of this check (30 secs at least). */
					vTaskGetInfo(pxTaskStatusArray[x].xHandle, &pxTaskStatusArray[x], pdTRUE, eSuspended);
					BOOT_FDIR_LOG_INFO("%s: %s f:%d", __func__,
										 pxTaskStatusArray[x].pcTaskName,
										 pxTaskStatusArray[x].usStackHighWaterMark);
				}
				boot_fdir.ts.get_task_status_info = now;
			}
			free_heap = xPortGetFreeHeapSize();
			BOOT_FDIR_LOG_INFO("%s: free heap %d B", __func__, free_heap);
		}

		if( BOOT_FDIR_TASK_FREE_HEAP_PERIOD < now-boot_fdir.ts.free_heap )
		{	/* Print the free heap left. */
			free_heap = xPortGetFreeHeapSize();
			BOOT_FDIR_LOG_INFO("%s: free heap %d B", __func__, free_heap);
			boot_fdir.ts.free_heap = now;
		}

		if( BOOT_FDIR_TASK_SANITY_LED_TOGGLE_PERIOD < now-boot_fdir.ts.sanity_led_toggle )
		{	/* Toggle led to debug sanity on the application. */
			API_GPIO_Toggle(API_GPIO_DBG_LED0);
			boot_fdir.ts.sanity_led_toggle = now;
		}

		if( BOOT_FDIR_TASK_FS_HEALTH_CHECK_PERIOD < now-boot_fdir.ts.fs_check )
		{ /* Run filesystem check. */
			boot_fdir_check_fs();
			boot_fdir.ts.fs_check = now;
		}

		vTaskDelay(50/portTICK_RATE_MS);
	}
	if( NULL != pxTaskStatusArray ) vPortFree(pxTaskStatusArray);
}

/*
 * @brief check the file system of the available volumes.
 * @return RET_OK if success.
 * */
static ret_code_t boot_fdir_check_fs(void)
{
	int fs_ret;
	ret_code_t ret = RET_OK;
	API_FS_statvfs_t statvfs = {0};
	char str_buff[128] = {0};
    char str_ts[API_DATETIME_TS_STR_LEN];
    char path[API_FS_DRIVE_STR_MAX_LEN+sizeof(BOOT_FDIR_CHECK_FILE_PATH)+5] = {0};
    uint32_t readed_bytes_testfile = 0;	/* will store the bytes readed from the test file. */
	uint64_t total_size = 0, free_size = 0;
    char *volumes_to_check[] =
	{
#ifdef API_FS_USE_SD
			API_FS_SD_DISK_PATH,
#endif
#ifdef API_FS_USE_MB85RS
			API_FS_FRAM0_DISK_PATH
#endif
	};
	uint8_t vol_count = sizeof(volumes_to_check)/sizeof(volumes_to_check[0]);

	for(int i = 0; i<sizeof(vol_count); i++)
	{
		{
			/* First check the free space. */
			fs_ret = API_FS_statvfs(volumes_to_check[i], &statvfs);
			if( 0 != fs_ret )
			{
				BOOT_FDIR_LOG_ERROR("%s:fs check err: retval=%d",__func__, ret);
			}
			else
			{
				total_size = (statvfs.total_sectors > 1024) ? ((statvfs.total_sectors/1024)*statvfs.sector_size) : ((statvfs.total_sectors*statvfs.sector_size)/1024);
				free_size = (statvfs.free_sectors > 1024) ? ((statvfs.free_sectors/1024)*statvfs.sector_size) : ((statvfs.free_sectors*statvfs.sector_size)/1024);
				BOOT_FDIR_LOG_INFO(	"%s:fs check ok: space: %lu/%lu kiB", __func__,
									free_size, total_size);
			}
			/* Then check if we can write to a file. */
			sprintf(path, "%s%s", volumes_to_check[i], BOOT_FDIR_CHECK_FILE_DIR);
			if( 0 == API_FS_mkdir(path) )
			{
				API_FS_FILE *test_file = NULL;
				sprintf(path, "%s%s", volumes_to_check[i], BOOT_FDIR_CHECK_FILE_PATH);
				/* First open the file to read its content. Its more for log purposes. */
				test_file = API_FS_fopen(path, "r");
				if( NULL != test_file )
				{
					readed_bytes_testfile = API_FS_fread(str_buff, sizeof(str_buff[0]), sizeof(str_buff), test_file);
					str_buff[readed_bytes_testfile] = '\0';
					BOOT_FDIR_LOG_INFO("%s: old health %s%s check: %s", __func__, volumes_to_check[i], BOOT_FDIR_CHECK_FILE_PATH, str_buff);
					if( 0 != API_FS_fclose(test_file) )
					{
						BOOT_FDIR_LOG_ERROR("%s: error closing %s%s in r", __func__, volumes_to_check[i], BOOT_FDIR_CHECK_FILE_PATH);
					}
				}
				/* Then overwrite the old text with the new one. */
				test_file = API_FS_fopen(path, "w");
				if( NULL != test_file )
				{
					/* Write the new data to health file. */
					API_Datetime_Get_Timestamp(str_ts, sizeof(str_ts));
					sprintf(str_buff,"%s: healthy fs check\r\n", str_ts);
					API_FS_fwrite(str_buff, sizeof(str_buff[0]), strlen(str_buff), test_file);
					BOOT_FDIR_LOG_INFO("%s: new health %s%s check: %s", __func__, volumes_to_check[i], BOOT_FDIR_CHECK_FILE_PATH, str_buff);
					if( 0 != API_FS_fclose(test_file) )
					{
						BOOT_FDIR_LOG_ERROR("%s: error closing %s%s in w", __func__, volumes_to_check[i], BOOT_FDIR_CHECK_FILE_PATH);
					}
				}
			}
		}
	}
	return ret;
}
