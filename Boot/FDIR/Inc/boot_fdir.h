/*
 * boot_fdir.h
 *
 *  Created on: Aug 16, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef BOOT_FDIR_H_
#define BOOT_FDIR_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"
#include "rtos.h"

// Public functions declaration -----------------------------------------------
TaskHandle_t boot_fdir_init(void *arg);

#endif /* FDIR_INC_BOOT_FDIR_H_ */
