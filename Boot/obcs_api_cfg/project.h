/*
 * project.h
 *
 *  Created on: Aug 5, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef PROJECT_H_
#define PROJECT_H_

/*
 * This header was used in older versions. In the new version, we must compile the project
 * with make defining the board we are targeting. Examples:
 * "make BOARD=OTB" <-- compile the project for the OTB board
 * "make BOARD=OS" <-- compile the project for the Open Space OBC board
 * */
//#define OTB
//#define OS_OBC

#endif /* OBC_API_CFG_PROJECT_H_ */
