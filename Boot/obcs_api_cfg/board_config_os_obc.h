/*
 * board_config_os_obc.c
 *
 *  Created on: Aug 5, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef BOARD_CONFIG_OS_OBC_C_
#define BOARD_CONFIG_OS_OBC_C_

// Includes -------------------------------------------------------------------
#include "board_cfg_types.h"

//#define UNIVERSITWIN_OBC_BROKEN_PB10	/*!< Uncomment this if you are using the broken PB10 pin OS OBC. */

// MCU ------------------------------------------------------------------------
#define STM32F407I	/*!< The MCU used. */

// GPIOs ----------------------------------------------------------------------
/* @brief number of GPIO used for digital outputs and inputs only and its purpose.*/
typedef enum
{
	API_GPIO_DBG_LED0 = 0,
	API_GPIO_DBG_LED1,
	API_GPIO_COND1,
	API_GPIO_COND2,
	API_GPIO_COND3,
	API_GPIO_PD_SS_5V,
	API_GPIO_PD_SS_3V3,
	API_GPIO_PD_SS_12V,
	API_GPIO_MAX		/*<! Number of GPIOs used. */
}API_GPIO_Port_Id_T;

#define BOARD_GPIO_SIZEOF_TABLE API_GPIO_MAX /*!< The number of elements in the board_gpio_handler_table.*/

#define BOARD_GPIO_DEBUG_LED0_GPORT		GPIOG
#define BOARD_GPIO_DEBUG_LED0_GPIN		GPIO_PIN_5
#define BOARD_GPIO_DEBUG_LED1_GPORT		GPIOG
#define BOARD_GPIO_DEBUG_LED1_GPIN		GPIO_PIN_6
#define BOARD_GPIO_COND1_GPORT			GPIOC
#define BOARD_GPIO_COND1_GPIN			GPIO_PIN_6
#define BOARD_GPIO_COND2_GPORT			GPIOG
#define BOARD_GPIO_COND2_GPIN			GPIO_PIN_7
#define BOARD_GPIO_COND3_GPORT			GPIOG
#define BOARD_GPIO_COND3_GPIN			GPIO_PIN_8
#define BOARD_PD_CTRL_PD_SS_5V_GPORT	GPIOH
#define BOARD_PD_CTRL_PD_SS_5V_GPIN		GPIO_PIN_15
#define BOARD_PD_CTRL_PD_SS_3V3_GPORT	GPIOI
#define BOARD_PD_CTRL_PD_SS_3V3_GPIN	GPIO_PIN_3
#define BOARD_PD_CTRL_PD_SS_12V_GPORT	GPIOI
#define BOARD_PD_CTRL_PD_SS_12V_GPIN	GPIO_PIN_2

// USARTs ---------------------------------------------------------------------
/* @brief number of USARTs used and its purpose. */
typedef enum
{
	API_USART_SERIAL_LOG = 0,	/*<! USART port used by the serial logger. */
	API_USART_MAX_PORTS			/*<! Number of USART ports used in the API. */
}API_Usart_Port_Id_T;

#define BOARD_USART_SIZEOF_TABLE API_USART_MAX_PORTS

#define BOARD_USART_SERIAL_LOG			USART1			/*!< USART used to send the serial logs by the board. Must be an USART instance. */
#define BOARD_USART_SERIAL_LOG_IRQN		USART1_IRQn		/*!< Interrupt Request number of the interrupt of the USART used.*/
#define BOARD_USART_SERIAL_LOG_APB		LL_APB2_GRP1_PERIPH_USART1
#define BOARD_USART_SERIAL_LOG_TX_GPORT	GPIOA
#define BOARD_USART_SERIAL_LOG_TX_GPIN	LL_GPIO_PIN_9	/*!< GPIO Pin used by USART TX. Must to be the LL driver of STM32Cube HAL definition. */
#define BOARD_USART_SERIAL_LOG_TX_AHB	LL_AHB1_GRP1_PERIPH_GPIOA
#define BOARD_USART_SERIAL_LOG_TX_AF	LL_GPIO_AF_7
#define BOARD_USART_SERIAL_LOG_RX_GPORT	GPIOA
#define BOARD_USART_SERIAL_LOG_RX_GPIN	LL_GPIO_PIN_10	/*!< GPIO Pin used by USART RX. Must to be the LL driver of STM32Cube HAL definition. */
#define BOARD_USART_SERIAL_LOG_RX_AHB	LL_AHB1_GRP1_PERIPH_GPIOA
#define BOARD_USART_SERIAL_LOG_RX_AF	LL_GPIO_AF_7

// Ethernet -------------------------------------------------------------------
#define BOARD_ETH_RMII_CLK_GPORT		GPIOA			/*!< Checked! */
#define BOARD_ETH_RMII_CLK_GPIN			GPIO_PIN_1		/*!< Checked! */
#define BOARD_ETH_RMII_MDIO_GPORT		GPIOA			/*!< Checked! */
#define BOARD_ETH_RMII_MDIO_GPIN		GPIO_PIN_2		/*!< Checked! */
#define BOARD_ETH_RMII_MDC_GPORT		GPIOC			/*!< Checked! */
#define BOARD_ETH_RMII_MDC_GPIN			GPIO_PIN_1		/*!< Checked! */
#define BOARD_ETH_RMII_CRSDV_GPORT		GPIOA			/*!< Checked! */
#define BOARD_ETH_RMII_CRSDV_GPIN		GPIO_PIN_7		/*!< Checked! */
#define BOARD_ETH_RMII_RX0_GPORT		GPIOC			/*!< Checked! */
#define BOARD_ETH_RMII_RX0_GPIN			GPIO_PIN_4		/*!< Checked! */
#define BOARD_ETH_RMII_RX1_GPORT		GPIOC			/*!< Checked! */
#define BOARD_ETH_RMII_RX1_GPIN			GPIO_PIN_5		/*!< Checked! */
#define BOARD_ETH_RMII_TX0_GPORT		GPIOB			/*!< Checked! */
#define BOARD_ETH_RMII_TX0_GPIN			GPIO_PIN_12		/*!< Checked! */
#define BOARD_ETH_RMII_TX1_GPORT		GPIOB			/*!< Checked! */
#define BOARD_ETH_RMII_TX1_GPIN			GPIO_PIN_13		/*!< Checked! */
#define BOARD_ETH_RMII_TXEN_GPORT		GPIOB			/*!< Checked! */
#define BOARD_ETH_RMII_TXEN_GPIN		GPIO_PIN_11		/*!< Checked! */
#define BOARD_ETH_RMII_RXERR_GPORT		GPIOH
#define BOARD_ETH_RMII_RXERR_GPIN		GPIO_PIN_2
#define BOARD_ETH_RST_GPORT				GPIOE
#define BOARD_ETH_RST_GPIN				GPIO_PIN_13
#define BOARD_ETH_INT_GPORT				GPIOE
#define BOARD_ETH_INT_GPIN				GPIO_PIN_14

// Micro SD SPI & Timers configuration ----------------------------------------
/* Micro SD SPI Chip Select. */
#define BOARD_SD_CS_GPORT			GPIOD
#define BOARD_SD_CS_GPIN			GPIO_PIN_13	/* Used by HAL driver functions. */
#define BOARD_SD_CS_GPIN_NUMBER		13			/* The number of the GPIO (used by diskio driver to make some things). */

/* Micro SD SPI MISO, MOSI & CLK. */
#define BOARD_SD_DISKIO_MISO_GPORT	GPIOB
#define BOARD_SD_DISKIO_MISO_GPIN	GPIO_PIN_14
#define BOARD_SD_DISKIO_MOSI_GPORT	GPIOB
#define BOARD_SD_DISKIO_MOSI_GPIN	GPIO_PIN_15

#if defined(UNIVERSITWIN_OBC_BROKEN_PB10)
/* NOTE: In one PCB of the Universitwin OBC, the PB10 pin is broken. For this reason
 * we must to exchange it for this pin. The original function of this pin is
 * CAM_SCCB_SCK. So this SCCB_SCK must be remaped to another pin if the CAM is
 * used with this PCB. */
#define BOARD_SD_DISKIO_CLK_GPORT	GPIOI
#define BOARD_SD_DISKIO_CLK_GPIN	GPIO_PIN_1
#else
#define BOARD_SD_DISKIO_CLK_GPORT	GPIOB
#define BOARD_SD_DISKIO_CLK_GPIN	GPIO_PIN_10
#endif

#define BOARD_SD_DISKIO_SPI_INSTANCE			SPI2	/*!< SPI used to handle the SD card. */
#define BOARD_SD_DISKIO_GPIO_AF					GPIO_AF5_SPI2
#define BOARD_SD_DISKIO_SPI_CLK_ENABLE() 		__HAL_RCC_SPI2_CLK_ENABLE()
#define BOARD_SD_DISKIO_CS_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOD_CLK_ENABLE()
#define BOARD_SD_DISKIO_MISO_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOB_CLK_ENABLE()
#define BOARD_SD_DISKIO_MOSI_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOB_CLK_ENABLE()
#define BOARD_SD_DISKIO_CLK_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOB_CLK_ENABLE()

/* SD Diskio main timer selection: */
#define BOARD_SD_DISKIO_MAIN_TIM					TIM2
#define BOARD_SD_DISKIO_MAIN_TIM_APB				APB1
#define BOARD_SD_DISKIO_MAIN_TIM_APB1_GRP1			LL_APB1_GRP1_PERIPH_TIM2
#define BOARD_SD_DISKIO_MAIN_TIM_CLK_SOURCE			LL_TIM_CLOCKSOURCE_INTERNAL
#define BOARD_SD_DISKIO_MAIN_TIM_IRQn				TIM2_IRQn
#define Board_SD_Diskio_Main_TIM_IRQHandler			TIM2_IRQHandler /* !< Re-definition of the ISR handler. Is implemented in the driver itself. */

/* SD Diskio second timer selection: */
#define BOARD_SD_DISKIO_SECONDARY_TIM				TIM5
#define BOARD_SD_DISKIO_SECONDARY_TIM_APB			APB1
#define BOARD_SD_DISKIO_SECONDARY_TIM_APB1_GRP1		LL_APB1_GRP1_PERIPH_TIM5
#define BOARD_SD_DISKIO_SECONDARY_TIM_CLK_SOURCE	LL_TIM_CLOCKSOURCE_INTERNAL
#define BOARD_SD_DISKIO_SECONDARY_TIM_IRQn			TIM5_IRQn
#define Board_SD_Diskio_Secondary_TIM_IRQHandler	TIM5_IRQHandler /* !< Re-definition of the ISR handler. Is implemented in the driver itself. */

#endif /* BOARD_CONFIG_OS_OBC_C_ */
