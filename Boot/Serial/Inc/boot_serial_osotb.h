/*
 * osotb_serial_definitions.h
 *
 *  Created on: Oct 10, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef OSOTB_SERIAL_DEFINITIONS_H_
#define OSOTB_SERIAL_DEFINITIONS_H_

ret_code_t boot_serial_osotb_init(void *arg);

#endif /* SERIAL_INC_OSOTB_SERIAL_DEFINITIONS_H_ */
