/*
 * osotb_serial_definitions.c
 *
 *  Created on: Oct 10, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
/* Tasks modules includes. */
#include "boot_app.h"

#include "api.h"	/* !< OBCs API include. */
// Macros & Constants ---------------------------------------------------------
#define	LOG_TAG						"boot_serial_osotb"	/*<! Tag assigned to logs for this module. */

#if BOOT_SERIAL_OSOTB_LOG_ENABLED
	#define BOOT_SERIAL_OSOTB_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_SERIAL_OSOTB_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_SERIAL_OSOTB_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_SERIAL_OSOTB_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define BOOT_SERIAL_OSOTB_LOG_ERROR(...)
	#define BOOT_SERIAL_OSOTB_LOG_WARNING(...)
	#define BOOT_SERIAL_OSOTB_LOG_INFO(...)
	#define BOOT_SERIAL_OSOTB_LOG_DEBUG(...)
#endif

// Private functions declarations ---------------------------------------------
/* Commands rx handlers: */
ret_code_t boot_serial_osotb_rx_handler_get_uptime(char *payload, char *payload_response);
ret_code_t boot_serial_osotb_rx_handler_get_def_app_slot(char *payload, char *payload_response);
ret_code_t boot_serial_osotb_rx_handler_put_def_app_slot(char *payload, char *payload_response);

// Private variables definition -----------------------------------------------
api_osotb_rx_handler_table_item_t serial_rx_table[] =
{
	API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(API_OSOTB_METHOD_GET, "uptime", boot_serial_osotb_rx_handler_get_uptime),
	API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(API_OSOTB_METHOD_GET, "def_app_slot", boot_serial_osotb_rx_handler_get_def_app_slot),
	API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(API_OSOTB_METHOD_PUT, "def_app_slot", boot_serial_osotb_rx_handler_put_def_app_slot)
};

#define OSOTB_RX_SIZEOF_TABLE (sizeof(serial_rx_table) / sizeof(serial_rx_table[0]))

api_osotb_rx_t serial_osotb_rx =
{
	.table = serial_rx_table,
	.table_size = OSOTB_RX_SIZEOF_TABLE,
};

// Public functions definitions -----------------------------------------------
/**
 * @brief init the osotb serial interface. Attach the resources/method/handlers
 * table to it.
 */
ret_code_t boot_serial_osotb_init(void *arg)
{
	ret_code_t ret = RET_OK;
	ret = api_osotb_serial_init(&serial_osotb_rx);
	return ret;
}

// Private functions definitions ----------------------------------------------
/* RX handlers of the OSOTB server --------------------------------------------*/
/**
 * @brief handler executed when a GET to the UPTIME resource is received.
 * @param payload in this case, the payload of the request is unused.
 * @param payload_response in this case, in the payload_response will be stored the
 * uptime of the devide.
 * @return RET_OK if success.
 */
ret_code_t boot_serial_osotb_rx_handler_get_uptime(char *payload, char *payload_response)
{
	if( NULL == payload_response ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	(void) payload;
	char timestamp_str[API_DATETIME_TS_STR_LEN];
	API_Datetime_Get_Timestamp(timestamp_str, sizeof(API_DATETIME_TS_STR_LEN));
	sprintf(payload_response, "%s(%lu)", timestamp_str, TICKS_TO_MS(xTaskGetTickCount()));
	return ret;
}

/**
 * @brief handler executed when a GET to the def_app_slot resource is received.
 * @param payload in this case, the payload of the request is unused.
 * @param payload_response in this case, in the payload_response will store the
 * default app_slot of the device.
 * @return RET_OK if success.
 */
ret_code_t boot_serial_osotb_rx_handler_get_def_app_slot(char *payload, char *payload_response)
{
	if( NULL == payload_response ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	(void) payload;
	api_iap_app_slot_t def_app_slot = api_iap_get_default_app();
	sprintf(payload_response, "%lu", (uint32_t) def_app_slot);
	return ret;
}

/**
 * @brief handler executed when a PUT to the def_app_slot resource is received.
 * @param payload in this case, the payload of request will have the default app slot to set.
 * @param payload_response in this case, returns the value flashed.
 * @return RET_OK if success.
 */
ret_code_t boot_serial_osotb_rx_handler_put_def_app_slot(char *payload, char *payload_response)
{
	if( NULL == payload_response ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;

	api_iap_app_slot_t app_slot = (api_iap_app_slot_t) atoi(payload);
	ret = api_iap_set_default_app(app_slot);
	if( RET_OK != ret )
	{
		BOOT_SERIAL_OSOTB_LOG_ERROR("%s: app_slot number not recognized! (app_slot %d)", __func__, app_slot);
	}
	else
	{
		BOOT_SERIAL_OSOTB_LOG_INFO("%s: app_slot flashed correctly! (app_slot %d)", __func__, app_slot);
	}

	// put in the payload_response the value burned in flash
	sprintf(payload_response, "%d", api_iap_get_default_app());

	return ret;
}
