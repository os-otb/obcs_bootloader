/*
 * boot_app.h
 *
 *  Created on: May 19, 2021
 *      Author: marifante
 */

#ifndef BOOT_APP_H_
#define BOOT_APP_H_

// Include --------------------------------------------------------------------
#include "api_types.h"
#include "rtos.h"

// Bootloader modules types
#include "boot_net_types.h"
#include "boot_app_types.h"

// Public functions declaration  ----------------------------------------------
TaskHandle_t boot_app_init(void *arg);

/* Functions used by other tasks to send msgs to the app task. */
ret_code_t boot_app_send_msg_sv_status(boot_app_msg_sv_status_t sv_status); /* !< Used by a server task to send information about it status to the app task. */
ret_code_t boot_app_send_msg_binary_received(boot_app_msg_bin_received_t bin_info); /* !< Used by a server task to notify to the app task that a binary file has been received. */

#endif /* BOOT_APP_H_ */
