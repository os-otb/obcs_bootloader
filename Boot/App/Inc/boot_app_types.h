/*
 * boot_app_types.h
 *
 *  Created on: Aug 19, 2021
 *      Author: marifante
 */

#ifndef BOOT_APP_TYPES_H_
#define BOOT_APP_TYPES_H_

// Macros & Constants ---------------------------------------------------------
#define BOOT_APP_FIL_ABS_PATH_MAX_LEN	32	/*<! The max length of the absolute path of a file advertise through a msg. */

// Public variable declaration ------------------------------------------------
typedef enum
{
  BOOT_APP_MSG_DEFAULT = 0,
  BOOT_APP_MSG_NET_SV_STATUS,	 	/*!< Message sent by the a net server task with the server status!. */
  BOOT_APP_MSG_NET_BIN_RECEIVED, 	/*!< Message sent by the net task when a binary file is received. */
}boot_app_msg_id_t;

/* @brief struct sent by a server task to the app task with the status of the server. */
typedef struct
{
	boot_net_server_id_t sv_id;
	bool sv_up;		/*!< The app only wants to know if the server is up (true) or if it is down (false). */
} boot_app_msg_sv_status_t;

/* @brief struct sent by a server task to the app task with the info of the received binary. */
typedef struct
{
	char file_name[BOOT_APP_FIL_ABS_PATH_MAX_LEN];	/*!< The absolute path to the binary file. */
} boot_app_msg_bin_received_t;

/**
 * @brief message packet format sent to the boot app task.
 * The data field is an union with the different data packets that can be sent
 * to the task.
 */
typedef struct //STRUCT_PACKET
{
	boot_app_msg_id_t id;
	union
	{
		boot_app_msg_sv_status_t sv_status;
		boot_app_msg_bin_received_t bin_info;
	}data;
}boot_app_msg_t;


#endif /* APP_INC_BOOT_APP_TYPES_H_ */
