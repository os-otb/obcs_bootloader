/*
 * boot_app_msg.c
 *
 *  Created on: Jun 6, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "boot_app_private.h" 	/* !< App module private include. */

// Private variables declaration ----------------------------------------------
static ret_code_t boot_app_send_msg(boot_app_msg_t *msg);
/* App Task Messages handlers prototypes: */
static void boot_app_process_msg_sv_status(boot_app_msg_t *msg);
static void boot_app_process_msg_binary_received(boot_app_msg_t *msg);

// Boot App Task message-handler table ----------------------------------------
/* App Task Messages handlers table: */
#define BOOT_APP_PROCESS_MSG(x,y) {.id = x, .handler=y}
static const boot_app_process_msg_table_t boot_app_process_msg_table[] =
{
  BOOT_APP_PROCESS_MSG(BOOT_APP_MSG_DEFAULT, NULL),
  BOOT_APP_PROCESS_MSG(BOOT_APP_MSG_NET_SV_STATUS, boot_app_process_msg_sv_status),
  BOOT_APP_PROCESS_MSG(BOOT_APP_MSG_NET_BIN_RECEIVED, boot_app_process_msg_binary_received),
};
#define SIZEOF_BOOT_APP_PROCESS_MSG_TABLE (sizeof(boot_app_process_msg_table) / sizeof(boot_app_process_msg_table[0]))

// Public functions definition  -----------------------------------------------
/*
 * @brief used by a net server task to send the status of the server to the
 * app task.
 * */
ret_code_t boot_app_send_msg_sv_status(boot_app_msg_sv_status_t sv_status)
{
	boot_app_msg_t msg;
	msg.data.sv_status.sv_id = sv_status.sv_id;
	msg.data.sv_status.sv_up = sv_status.sv_up;
	msg.id = BOOT_APP_MSG_NET_SV_STATUS;
	return boot_app_send_msg(&msg);
}

/**
 * @brief used by another task to inform to the app task that a file has been received.
 */
ret_code_t boot_app_send_msg_binary_received(boot_app_msg_bin_received_t file_info)
{
	boot_app_msg_t msg;
	if(NULL != file_info.file_name) strcpy(msg.data.bin_info.file_name, file_info.file_name);
	msg.id = BOOT_APP_MSG_NET_BIN_RECEIVED;
	return boot_app_send_msg(&msg);
}

/**
 * @brief send a message to the app task through its queue.
 * This function can be called from an ISR too.
 * */
static ret_code_t boot_app_send_msg(boot_app_msg_t *msg)
{
	BaseType_t rtos_result = pdTRUE;
	ret_code_t res = RET_OK;

	if( (NULL == boot_app.cmd_queue) || (NULL == msg) )
	{
		return RET_ERR;
	}
	else
	{
		if(pdTRUE != xPortIsInsideInterrupt())
		{
			/* If the core isn't in an interrupt context then send the msg normally. */
			rtos_result = xQueueSend( boot_app.cmd_queue, msg, ( TickType_t ) 10 );
		}
		else
		{
				/* If the core is in an interrupt context then use send from ISR API. */
			portBASE_TYPE xHigherPriorityTaskWoken;
			BaseType_t yield_req = pdFALSE;

			rtos_result = xQueueSendFromISR( boot_app.cmd_queue, msg, &xHigherPriorityTaskWoken );
			// Now the buffer is empty we can switch context if necessary.
			if( xHigherPriorityTaskWoken )
			{
			  // Actual macro used here is port specific.
			  portYIELD_FROM_ISR(yield_req);
			}
		}
		res = (pdTRUE == rtos_result) ? RET_OK : RET_ERR;
		return res;
	}
}

// Private functions definition -----------------------------------------------
/**
 *
 * @brief  process incoming queue messages. This function calls the handler
 * associated with that message.
 *
 * @param msg queue message
 *
 */
void boot_app_process_msg(boot_app_msg_t *msg)
{
	uint8_t c;
	if( NULL != msg )
	{
		for(c = 0; c < SIZEOF_BOOT_APP_PROCESS_MSG_TABLE; c++)
		{
			if(boot_app_process_msg_table[c].id == msg->id)
			{
				// found, check callback
				if(NULL != boot_app_process_msg_table[c].handler)
				{
					// call it
					boot_app_process_msg_table[c].handler(msg);
				}
				// finish search
				break;
			}
		}
	}
}

/**
 * @brief another task advised to app task that the device is connected
 * to the network!
 * */
static void boot_app_process_msg_sv_status(boot_app_msg_t *msg)
{
	if( NULL != msg )
	{
		if( (BOOT_NET_FTP_SERVER_ID == msg->data.sv_status.sv_id ) &&
			(true == msg->data.sv_status.sv_up))
		{
			 boot_app.fsm_extended_vars.init.sv_status.ftp_up = true;
		}

		if( (BOOT_NET_OSOTB_SERVER_ID == msg->data.sv_status.sv_id ) &&
			(true == msg->data.sv_status.sv_up))
		{
			 boot_app.fsm_extended_vars.init.sv_status.osotb_up = true;
		}
	}
	else
	{
		BOOT_APP_LOG_ERROR("%s: msg is null!", __func__);
	}
}

/**
 * @brief another task advised to app task that a binary
 * has been received, so trigger the correspondent event if the
 * filename is valid.
 * */
static void boot_app_process_msg_binary_received(boot_app_msg_t *msg)
{
	if( (NULL == msg) || (NULL == msg->data.bin_info.file_name) ) return;
	upgrade_fw_on_entry_arg_t ev_argv = {0};
	if( 0 == strcmp(msg->data.bin_info.file_name, API_IAP_APP0_ABS_PATH) )
	{
		ev_argv.app_slot = 0; /* Flash APP0 slot. */
		api_fsm_process_event(&boot_app.fsm, BOOT_APP_EV_VALID_BINARY_RX, (void*) &ev_argv);
		BOOT_APP_LOG_WARNING("%s: binary received is from app0 (%s == %s)!", __func__, msg->data.bin_info.file_name, API_IAP_APP0_ABS_PATH);
	}
	else
	if( 0 == strcmp(msg->data.bin_info.file_name, API_IAP_APP1_ABS_PATH) )
	{
		ev_argv.app_slot = 1; /* Flash APP1 slot. */
		api_fsm_process_event(&boot_app.fsm, BOOT_APP_EV_VALID_BINARY_RX, (void*) &ev_argv);
		BOOT_APP_LOG_WARNING("%s: binary received is from app1 (%s == %s)!", __func__, msg->data.bin_info.file_name, API_IAP_APP1_ABS_PATH);
	}
	else
	{
		BOOT_APP_LOG_WARNING("%s: the binary received is not a valid binary for flash (%s)!", __func__, msg->data.bin_info.file_name);
	}
}

