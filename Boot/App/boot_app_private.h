/*
 * boot_app_private.h
 *
 *  Created on: Jun 6, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef BOOT_APP_PRIVATE_H_
#define BOOT_APP_PRIVATE_H_

// Includes -------------------------------------------------------------------
/* Tasks modules includes. */
#include "boot_app.h"
#include "boot_net.h"

#include "api.h"	/* !< OBCs API include. */

// Macros and constants -------------------------------------------------------
/* Appwork config. TODO move to another place? */

#define BOOT_APP_LOG_ENABLED			1		/*<! Enables module log. */

#define	LOG_TAG						"boot_app"	/*<! Tag assigned to logs for this module. */

#if BOOT_APP_LOG_ENABLED
	#define BOOT_APP_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_APP_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_APP_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_APP_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define BOOT_APP_LOG_ERROR(...)
	#define BOOT_APP_LOG_WARNING(...)
	#define BOOT_APP_LOG_INFO(...)
	#define BOOT_APP_LOG_DEBUG(...)
#endif

// Private variables declaration ----------------------------------------------
typedef struct
{
	TaskHandle_t task_handle;
	QueueHandle_t cmd_queue;  	/* !< Other tasks or ISR will notify app task about some events with this queue. */

	/* This struct have the extended vars of the states of the fsm. With this vars, certain
	 * states of the fsm will do some operations to evaluate the flow of the operation. */
	struct
	{
		struct
		{
			struct
			{
				bool ftp_up;	/*!< To store when the ftp server is up!. */
				bool osotb_up;	/*!< To store when the osotb server is up!. */
			}sv_status;
			int entry_time; /*!< To take in count the time from the most recent entry in the state. */
		}init;
		struct
		{
			api_iap_app_slot_t app_slot; /*!< The app slot to be flashed. */
			int init_time; /*!< To take in count the total time the fsm has been in the state. */
		}fw_upgrade;
	}fsm_extended_vars;
	api_fsm_t fsm;	/* !< Struct that holds the information of fsm of this module. */
} boot_app_t;

typedef struct
{
  uint8_t id;
  void (*handler)(boot_app_msg_t *msg);
}boot_app_process_msg_table_t;

// Private variables definition -----------------------------------------------
boot_app_t boot_app;

// Private functions declaration ----------------------------------------------
void boot_app_process_msg(boot_app_msg_t *msg);

// Module finite state machine declarations -----------------------------------
typedef enum
{
    BOOT_APP_STATE_INIT = 0,
	BOOT_APP_STATE_IDLE,
	BOOT_APP_STATE_UPGRADE_FW,
    BOOT_APP_MAX_STATES			/*!< Max states on the fsm. */
}boot_app_fsm_state_id_t;

/* States handlers, types & constants declarations: */
/* BOOT_APP_STATE_INIT */
#define BOOT_APP_INIT_STATE_RETRY_TIMEOUT_MS 10000
ret_code_t boot_app_fsm_init_on_entry_handler(void *arg);
ret_code_t boot_app_fsm_init_ongoing_handler(void *arg);

/* BOOT_APP_STATE_UPGRADE_FW */
#define BOOT_APP_FW_UPGRADE_STATE_TIMEOUT_MS	30000
typedef struct
{
	api_iap_app_slot_t app_slot; /*!< The app slot to update (0 or 1). */
}upgrade_fw_on_entry_arg_t;
ret_code_t boot_app_fsm_upgrade_fw_on_entry_handler(void *arg);
ret_code_t boot_app_fsm_upgrade_fw_ongoing_handler(void *arg);

/*
 * @brief events that will trigger transitions in this fsm:
 * */
typedef enum
{
    BOOT_APP_EV_DEV_CONN = 0,		/*!< The device is connected to the network and the servers are created. */
	BOOT_APP_EV_RETRY_INIT,			/*!< Retry the init process... */
	BOOT_APP_EV_VALID_BINARY_RX,	/*!< A valid binary has been received. */
    BOOT_APP_EV_FLASHING_PROCESS_FINISHED,	/*!< The flashing process finished (successfull or not). */
	BOOT_APP_EV_FLASHING_PROCESS_TIMEOUT, /*!< A timeout ocurred in the flashing process. */
	BOOT_APP_EV_FLASHING_PROCESS_RETRY_REQUIRED, /*!< The flashing process failed and the timeout has not been reached. Retry needed. */
	BOOT_APP_EV_OP_MODE_CMD_IDLE, /*!< Command to change the op mode to idle!... */
	BOOT_APP_MAX_EVENTS			/*!< Max events on the fsm. */
}boot_app_fsm_state_events_t;

#endif /* BOOT_APP_PRIVATE_H_ */
