/*
 * board.c
 *
 *  Created on: March 15, 2021
 *      Authors:
 *      Lucas Mancini
 *      Julian Rodriguez aka marifante
 */

// Include --------------------------------------------------------------------
#include "boot_app_private.h" 	/* !< App module private include. */

// Macros & constants ---------------------------------------------------------
#define BOOT_APP_TASK_DELAY_MS			50			/*<! in ms, delay of the task. */
#define BOOT_APP_TASK_STACK_SIZE		512
#define BOOT_APP_TASK_PRIOTY			osPriorityNormal
#define BOOT_APP_QUEUE_TIMEOUT			(10 / portTICK_PERIOD_MS)
#define BOOT_APP_QUEUE_LEN				10

// Private functions declaration ----------------------------------------------
void boot_app_task(void *arg);

// Private variables definition -----------------------------------------------

/* States of the fsm of this module: */
api_fsm_state_t boot_app_fsm_states[] =
{
	API_FSM_DEFINE_STATE(	BOOT_APP_STATE_INIT,
							boot_app_fsm_init_on_entry_handler,
							boot_app_fsm_init_ongoing_handler,
							NULL),
	API_FSM_DEFINE_STATE(BOOT_APP_STATE_IDLE,
							NULL,
							NULL,
							NULL),
	API_FSM_DEFINE_STATE(BOOT_APP_STATE_UPGRADE_FW,
							boot_app_fsm_upgrade_fw_on_entry_handler,
							boot_app_fsm_upgrade_fw_ongoing_handler,
							NULL),
};

/* Transitions of the fsm of this module: */
api_fsm_transition_t boot_app_fsm_transitions_map[] =
{
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_APP_STATE_INIT, BOOT_APP_EV_RETRY_INIT, BOOT_APP_STATE_INIT),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_APP_STATE_INIT, BOOT_APP_EV_DEV_CONN, BOOT_APP_STATE_IDLE),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_APP_STATE_INIT, BOOT_APP_EV_OP_MODE_CMD_IDLE, BOOT_APP_STATE_IDLE),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_APP_STATE_IDLE, BOOT_APP_EV_VALID_BINARY_RX, BOOT_APP_STATE_UPGRADE_FW),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_APP_STATE_UPGRADE_FW, BOOT_APP_EV_FLASHING_PROCESS_FINISHED, BOOT_APP_STATE_IDLE),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_APP_STATE_UPGRADE_FW, BOOT_APP_EV_FLASHING_PROCESS_TIMEOUT, BOOT_APP_STATE_IDLE),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_APP_STATE_UPGRADE_FW, BOOT_APP_EV_FLASHING_PROCESS_RETRY_REQUIRED, BOOT_APP_STATE_UPGRADE_FW),
};

// Public functions definition  -----------------------------------------------
/**
 * @brief init app module of bootloader.
 * @return RET_OK if success.
 * */
TaskHandle_t boot_app_init(void *arg)
{
	/* Init fsm struct: */
	boot_app.fsm.actual_state = &boot_app_fsm_states[0];
	boot_app.fsm.previous_state = &boot_app_fsm_states[0];
	boot_app.fsm.state_table = boot_app_fsm_states;
	boot_app.fsm.transition_map = boot_app_fsm_transitions_map;
	boot_app.fsm.states_qty = BOOT_APP_MAX_STATES;
	boot_app.fsm.transitions_qty = BOOT_APP_MAX_EVENTS;

	/* Create commands queue and task: */
	boot_app.cmd_queue = xQueueCreate(BOOT_APP_QUEUE_LEN, sizeof(boot_app_msg_t));
	if( NULL != boot_app.cmd_queue )
	{
		if( xTaskCreate(	boot_app_task, "boot_app",
							BOOT_APP_TASK_STACK_SIZE,
							NULL,
							BOOT_APP_TASK_PRIOTY,
							&boot_app.task_handle ) != pdPASS )
		{
			/* If the task can't be created the task handle will be NULL. */
			boot_app.task_handle = NULL;
		}
	}
	return boot_app.task_handle;
}

// Private functions definition -----------------------------------------------
void boot_app_task(void *arg)
{
	boot_app_msg_t msg;

	if( 0 != API_FS_mount(API_IAP_BIN_DISK_PATH) )
	{
		BOOT_APP_LOG_ERROR("%s: cant mount %s",__func__, API_IAP_BIN_DISK_PATH);
	}
	else
	{
		BOOT_APP_LOG_INFO("%s: %s mounted",__func__, API_IAP_BIN_DISK_PATH);
	}

	while(true)
	{
		if(pdTRUE == xQueueReceive(boot_app.cmd_queue, (uint8_t*)&msg, BOOT_APP_QUEUE_TIMEOUT))
		{
			boot_app_process_msg(&msg); // process incoming message
		}
		api_fsm_work(&boot_app.fsm, NULL); // Process the actual state of the fsm
	}
}

/* Definition of the handlers of the fsm: */
/* BOOT_APP_FSM_STATE_IDLE functions ----------------------------------------*/
/*
 * @brief sends a message to the net task saying it must init the ftp server!
 * */
ret_code_t boot_app_fsm_init_on_entry_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	boot_app.fsm_extended_vars.init.entry_time = TICKS_TO_MS(xTaskGetTickCount());

	ret = boot_net_send_msg_query_sv_status(BOOT_NET_FTP_SERVER_ID);
	if( RET_OK != ret )
	{
		BOOT_APP_LOG_ERROR("%s: error sending message to net ftp task");
	}

	ret = boot_net_send_msg_query_sv_status(BOOT_NET_OSOTB_SERVER_ID);
	if( RET_OK != ret )
	{
		BOOT_APP_LOG_ERROR("%s: error sending message to net osotb task");
	}

	if( (true == boot_app.fsm_extended_vars.init.sv_status.ftp_up) &&
		(true == boot_app.fsm_extended_vars.init.sv_status.osotb_up) )
	{
		BOOT_APP_LOG_INFO("%s: The servers are up! continue.", __func__);
		api_fsm_process_event(&boot_app.fsm, BOOT_APP_EV_DEV_CONN, NULL);
	}

	return ret;
}

/*
 * @brief if the timeout has been reached, retry the init process. An event
 * triggered from the net task will send this fsm to the idle state.
 * */
ret_code_t boot_app_fsm_init_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	int now = TICKS_TO_MS(xTaskGetTickCount());
	if( BOOT_APP_INIT_STATE_RETRY_TIMEOUT_MS < (now - boot_app.fsm_extended_vars.init.entry_time) )
	{
		ret = api_fsm_process_event(&boot_app.fsm, BOOT_APP_EV_RETRY_INIT, NULL);
	}
	return ret;
}

/* BOOT_APP_FSM_STATE_UPGRADE_FW functions ----------------------------------*/
/*
 * @brief on_entry handler of the upgrade_fw state.
 * @details this functions threats its *arg as a pointer
 * to a upgrade_fw_on_entry_arg_t struct. This struct
 * must have got the filename and the app slot for the binary to
 * be flashed.
 * */
ret_code_t boot_app_fsm_upgrade_fw_on_entry_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	upgrade_fw_on_entry_arg_t *ev_argv = NULL;
	ev_argv = (upgrade_fw_on_entry_arg_t *) arg;

	if( NULL != ev_argv )
	{
		boot_app.fsm_extended_vars.fw_upgrade.app_slot = ev_argv->app_slot;
	}
	else
	{
		BOOT_APP_LOG_ERROR("%s: null ptr passed as param!", __func__);
		ret = RET_ERR_NULL_POINTER;
	}

	/* Store the initial time of this state. */
	boot_app.fsm_extended_vars.fw_upgrade.init_time = TICKS_TO_MS(xTaskGetTickCount());

	return ret;
}

/*
 * @brief ongoing handler of the upgrade_fw state.
 * @details this function takes in count the total time the fsm
 * takes in this state from the last transition. This function can trigger
 * 3 possible events:
 * 1. If upgrade process fails and the timeout is not reached then retry.
 * 2. If upgrade process fails and the timeout is reached then timeout and go to idle.
 * 3. If upgrade process success then go to idle.
 * */
ret_code_t boot_app_fsm_upgrade_fw_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	char bin_abs_path[API_FS_FILE_NAME_MAX_LEN];
	int now = TICKS_TO_MS(xTaskGetTickCount());

	if( API_IAP_APP0_SLOT == boot_app.fsm_extended_vars.fw_upgrade.app_slot )
	{
		strcpy(bin_abs_path,  API_IAP_APP0_ABS_PATH);
	}
	else
	if( API_IAP_APP1_SLOT == boot_app.fsm_extended_vars.fw_upgrade.app_slot )
	{
		strcpy(bin_abs_path,  API_IAP_APP1_ABS_PATH);
	}
	else
	{
		BOOT_APP_LOG_ERROR("%s: binary slot not recognized! s=%d", __func__, boot_app.fsm_extended_vars.fw_upgrade.app_slot);
	}

	if( (now-boot_app.fsm_extended_vars.fw_upgrade.init_time) < BOOT_APP_FW_UPGRADE_STATE_TIMEOUT_MS )
	{
		ret = api_iap_program(boot_app.fsm_extended_vars.fw_upgrade.app_slot, bin_abs_path);
		if( RET_OK != ret )
		{
			BOOT_APP_LOG_ERROR("%s: error in api_iap_program! app_slot: %2.d filename: %s",
					__func__, boot_app.fsm_extended_vars.fw_upgrade.app_slot, bin_abs_path);
		}
		else
		{	// The flashing process was successful! exit with success
			BOOT_APP_LOG_INFO("%s: FW upgrade process of app%d has finished correctly!",
					__func__, boot_app.fsm_extended_vars.fw_upgrade.app_slot);
			api_fsm_process_event(&boot_app.fsm, BOOT_APP_EV_FLASHING_PROCESS_FINISHED, NULL);
		}
		now = TICKS_TO_MS(xTaskGetTickCount()); // update ts
	}
	else
	{	// The flashing process could not be done... exit with timeout
		api_fsm_process_event(&boot_app.fsm, BOOT_APP_EV_FLASHING_PROCESS_TIMEOUT, NULL);
	}

	return ret;
}
