/*
 * boot_net_ftp.c
 *
 *  Created on: Aug 18, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "boot_net_private.h" 	/* !< Net module private include. */

// Macros & Constants ---------------------------------------------------------
#define BOOT_NET_FTP_TASK_DELAY_MS			50			/*<! in ms, delay of the task. */
#define BOOT_NET_FTP_TASK_STACK_SIZE		1024
#define BOOT_NET_FTP_TASK_PRIOTY			osPriorityNormal
#define BOOT_NET_FTP_QUEUE_TIMEOUT			(10 / portTICK_PERIOD_MS)
#define BOOT_NET_FTP_QUEUE_LEN				10

/* Network config. TODO move to another place? */
#define BOOT_NET_FTP_SERVER_PORT				49152
#define BOOT_NET_FTP_LOG_ENABLED				1		/*<! Enables module log. */
#define	LOG_TAG						"boot_net_ftp"	/*<! Tag assigned to logs for this module. */

#if BOOT_NET_FTP_LOG_ENABLED
	#define BOOT_NET_FTP_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_NET_FTP_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_NET_FTP_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_NET_FTP_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define BOOT_NET_FTP_LOG_ERROR(...)
	#define BOOT_NET_FTP_LOG_WARNING(...)
	#define BOOT_NET_FTP_LOG_INFO(...)
	#define BOOT_NET_FTP_LOG_DEBUG(...)
#endif

// Private functions declaration ----------------------------------------------
ret_code_t boot_net_ftp_fsm_sv_work_ongoing_handler(void *arg);
ret_code_t boot_net_ftp_fsm_sv_create_ongoing_handler(void *arg);
ret_code_t boot_net_ftp_fsm_sv_send_ongoing_handler(void *arg);

// Private variables declaration ----------------------------------------------
/* States of the fsm of this module: */
api_fsm_state_t boot_net_ftp_fsm_states[] =
{
	API_FSM_DEFINE_STATE(	BOOT_NET_FSM_SV_DISCONNECTED,
							NULL,
							NULL,
							NULL),
	API_FSM_DEFINE_STATE(	BOOT_NET_FSM_SV_CREATE,
							NULL,
							boot_net_ftp_fsm_sv_create_ongoing_handler,
							NULL),
	API_FSM_DEFINE_STATE(	BOOT_NET_FSM_SV_WORK,
							NULL,
							boot_net_ftp_fsm_sv_work_ongoing_handler,
							NULL),
	API_FSM_DEFINE_STATE(	BOOT_NET_FSM_SV_SEND,
							NULL,
							boot_net_ftp_fsm_sv_send_ongoing_handler,
							NULL),
};

/* Transitions of the fsm of this module: */
api_fsm_transition_t boot_net_ftp_fsm_transitions_map[] =
{
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_DISCONNECTED, BOOT_NET_EV_SV_CONNECTED, BOOT_NET_FSM_SV_CREATE),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_CREATE, BOOT_NET_EV_SV_CREATED, BOOT_NET_FSM_SV_WORK),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_WORK, BOOT_NET_EV_SV_SEND_DATA_REQUEST, BOOT_NET_FSM_SV_SEND),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_SEND, BOOT_NET_EV_SV_DATA_TX_SUCCESS, BOOT_NET_FSM_SV_WORK),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_SEND, BOOT_NET_EV_SV_DATA_TX_TIMEOUT, BOOT_NET_FSM_SV_WORK),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_SEND, BOOT_NET_EV_SV_DISCONNECTED, BOOT_NET_FSM_SV_DISCONNECTED),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_WORK, BOOT_NET_EV_SV_DISCONNECTED, BOOT_NET_FSM_SV_DISCONNECTED),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_CREATE, BOOT_NET_EV_SV_DISCONNECTED, BOOT_NET_FSM_SV_DISCONNECTED),
};

// Private variables definition -----------------------------------------------

// Public functions definition  -----------------------------------------------
/*
 * @brief initalize the ftp server task.
 * @return RET_OK if success.
 * */
ret_code_t boot_net_ftp_init(void)
{
	ret_code_t ret = RET_ERR;
	memset(&boot_net_ftp, 0, sizeof(boot_net_ftp));
	boot_net_ftp.sv_id = BOOT_NET_FTP_SERVER_ID;

	boot_net_ftp.fsm.actual_state = &boot_net_ftp_fsm_states[0];
	boot_net_ftp.fsm.previous_state = &boot_net_ftp_fsm_states[0];
	boot_net_ftp.fsm.state_table = boot_net_ftp_fsm_states;
	boot_net_ftp.fsm.transition_map = boot_net_ftp_fsm_transitions_map;
	boot_net_ftp.fsm.states_qty = BOOT_NET_FSM_MAX_STATES;
	boot_net_ftp.fsm.transitions_qty = BOOT_NET_MAX_EVENTS;

 	boot_net_ftp.task.cmd_queue = xQueueCreate(BOOT_NET_FTP_QUEUE_LEN, sizeof(boot_net_msg_t));
	if( NULL != boot_net_ftp.task.cmd_queue )
	{
		// TODO check if the queue was created
		if( xTaskCreate(	boot_net_task_template, "boot_net_ftp",
							BOOT_NET_FTP_TASK_STACK_SIZE,
							&boot_net_ftp,	/*!< Pass a pointer to the server struct to the task function to handle the server fsm. */
							BOOT_NET_FTP_TASK_PRIOTY,
							&boot_net_ftp.task.handle ) != pdPASS )
		{
			/* If the task can't be created the task handle will be NULL. */
			boot_net_ftp.task.handle = NULL;
		}
		else
		{
			ret = RET_OK;
		}
	}
	return ret;
}

// Private functions definition -----------------------------------------------
/**
 * @brief callback of the FTP API. This callback is called every time that a
 * file is received!.
 * @param file_name is the absolute path of the file.
 * @param reply_msg reply message is the
 */
void api_ftp_file_received_cb(const char *file_name, char *reply_msg)
{
	if( NULL != file_name )
	{
	    if( ( 0 == strcmp(file_name, API_IAP_APP0_ABS_PATH)) ||
			( 0 == strcmp(file_name, API_IAP_APP1_ABS_PATH)) )
	    { /* If the file is a binary file with a valid name. */
	      boot_net_ftp.fsm_extended_vars.ftp.sv_work.bin_rx = true;
	      strcpy(boot_net_ftp.fsm_extended_vars.ftp.sv_work.filename, file_name);
	      sprintf(reply_msg, "200-Binary %s received!\r\n", file_name);
	      BOOT_NET_FTP_LOG_INFO("%s: valid binary received! (%s)", __func__, file_name);
	    }
	    else
	    {
	      // TODO: delete file?
		  sprintf(reply_msg, "200-Incorrect filename (%s)!", file_name);
	      BOOT_NET_FTP_LOG_WARNING("%s: the binary received is not a valid binary for flash (%s)!", __func__, file_name);
	    }
	}
}

/* Task Message handlers ----------------------------------------------------*/
/**
 * @brief  process BOOT_NET_MSG_NET_DOWN_EVENT message.
 * @param msg queue message
 */
void boot_net_ftp_process_msg_net_down_event(boot_net_msg_t *msg)
{
	BOOT_NET_FTP_LOG_WARNING("Device disconnected from the network.");
	/* Trigger the event that says to the FSM that the device is disconnected
	 * from the network. */
	api_fsm_process_event(&boot_net_ftp.fsm, BOOT_NET_EV_SV_DISCONNECTED, NULL);
}

/**
 * @brief  process BOOT_NET_MSG_NET_UP_EVENT message.
 * @param msg queue message
 */
void boot_net_ftp_process_msg_net_up_event(boot_net_msg_t *msg)
{
	api_fsm_process_event(&boot_net_ftp.fsm, BOOT_NET_EV_SV_CONNECTED, NULL);
}

/**
 * @brief process a query of the status of the server.
 */
void boot_net_ftp_process_msg_query_sv_status(boot_net_msg_t *msg)
{
	if( NULL != msg )
	{
		if( (BOOT_NET_FSM_SV_WORK == boot_net_ftp.fsm.actual_state->id) ||
			(BOOT_NET_FSM_SV_SEND == boot_net_ftp.fsm.actual_state->id) )
		{
			boot_app_msg_sv_status_t sv_status = {0};
			sv_status.sv_id = BOOT_NET_FTP_SERVER_ID;
			sv_status.sv_up = true;
			boot_app_send_msg_sv_status(sv_status);
		}
	}
	else
	{
		BOOT_NET_FTP_LOG_ERROR("%s: msg is null!", __func__);
	}
}

/**
 * @brief process BOOT_NET_MSG_QUERY_SV_STATUS message.
 * @param msg queue message
 * */
void boot_net_ftp_process_msg_send_file(boot_net_msg_t *msg)
{
	/*TODO implement this*/
}

/* FSM on_entry, ongoing and on_exit handlers -------------------------------*/
/*
 * @brief in this state, the task tries to create the server continuosly. And if
 * the server is created sucessfully, then go to the next state.
 * */
ret_code_t boot_net_ftp_fsm_sv_create_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	ret = API_FTP_Server_Create();
	if( RET_OK == ret )
	{
		BOOT_NET_FTP_LOG_INFO("%s: FTP server created!", __func__);
		api_fsm_process_event(&boot_net_ftp.fsm, BOOT_NET_EV_SV_CREATED, NULL);
	}
	return ret;
}

/*
 * @brief do a ftp server working cycle.
 * */
ret_code_t boot_net_ftp_fsm_sv_work_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	ret = API_FTP_Server_Work();
	if( RET_OK == ret )
	{
		if( true == boot_net_ftp.fsm_extended_vars.ftp.sv_work.bin_rx )
		{	/* If a file has been received, the received file callback will set this flag
		 	 * and store the name of the file. */
			boot_app_msg_bin_received_t bin_info = {0};
			strcpy(bin_info.file_name, boot_net_ftp.fsm_extended_vars.ftp.sv_work.filename);
			boot_app_send_msg_binary_received(bin_info);
			boot_net_ftp.fsm_extended_vars.ftp.sv_work.bin_rx = false;
		}
	}
	return ret;
}

/*
 * @brief N/A
 * TODO this must be implemented.
 * */
ret_code_t boot_net_ftp_fsm_sv_send_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	// TODO implement this
	return ret;
}
