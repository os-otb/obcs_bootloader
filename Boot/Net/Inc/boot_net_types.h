/*
 * boot_net_types.h
 *
 *  Created on: Aug 19, 2021
 *      Author: marifante
 */

#ifndef BOOT_NET_TYPES_H_
#define BOOT_NET_TYPES_H_

// Public variable declaration ------------------------------------------------
/* @brief the servers that can be handled by this module. */
typedef enum
{
	BOOT_NET_FTP_SERVER_ID = 0,
	BOOT_NET_OSOTB_SERVER_ID,
	BOOT_NET_MAX_SERVERS 				/*!< Last one. Max quantity of states. */
} boot_net_server_id_t;

/* @brief the messages that can process from another tasks the servers of this
 * module. */
typedef enum
{
  BOOT_NET_MSG_DEFAULT = 0,
  BOOT_NET_MSG_NET_UP_EVENT,		/*!< Used by FreeRTOS+TCP TCP/IP stack to inform if the device is connected to the net. */
  BOOT_NET_MSG_NET_DOWN_EVENT,		/*!< Used by FreeRTOS+TCP TCP/IP stack to inform if the device was disconnected from the net. */
  BOOT_NET_MSG_PING_RESPONSE,		/*!< Used by FreeRTOS+TCP TCP/IP stack to inform that a ping response has been received. */
  BOOT_NET_MSG_SEND,			/*!< Used by another task to indicate to the server that must to send data to the client. */
  BOOT_NET_MSG_QUERY_SV_STATUS,	/*!< Used by another tasks to query the server status. */
  /* FTP serve task proper msg... */
  BOOT_NET_FTP_MSG_BIN_RX,	/*!< Used by the proper task to send a m*/
  BOOT_NET_MAX_MSGS /*last one*/
}boot_net_msg_id_t;

/* The format of the packets sent by other tasks to the server tasks. */
/* @brief this packet is sent by the FreeRTOS+TCP task to the server task when
 * is connected to the network. */
typedef struct
{
	uint32_t ulIPAddress;
	uint32_t ulNetMask;
	uint32_t ulGatewayAddress;
	uint32_t ulDNSServerAddress;
}boot_net_msg_data_net_up_event_t;

/* @brief this packet is sent by the FreeRTOS+TCP task to the server task when
 * is a echo reply to a previously echo request was received. */
typedef struct
{
	uint16_t echo_reply_id;
	ePingReplyStatus_t status;
}boot_net_msg_data_ping_response_t;

/* @brief the struct that must be filled by a task if wants to send a file
 * through the FTP server. */
typedef struct
{
	char *file_path; /* !< The path of the file to send to the client. */
}boot_net_ftp_msg_send_data_t;

#endif /* NET_INC_BOOT_NET_TYPES_H_ */
