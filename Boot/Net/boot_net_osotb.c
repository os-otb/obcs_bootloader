/*
 * boot_net_osotb.c
 *
 *  Created on: Aug 18, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "boot_net_private.h" 	/* !< Net module private include. */

// Macros & Constants ---------------------------------------------------------
#define BOOT_NET_OSOTB_TASK_DELAY_MS			50			/*<! in ms, delay of the task. */
#define BOOT_NET_OSOTB_TASK_STACK_SIZE		1024
#define BOOT_NET_OSOTB_TASK_PRIOTY			osPriorityNormal
#define BOOT_NET_OSOTB_QUEUE_TIMEOUT			(10 / portTICK_PERIOD_MS)
#define BOOT_NET_OSOTB_QUEUE_LEN				10

/* Network config. TODO move to another place? */
#define BOOT_NET_OSOTB_SERVER_PORT				49152
#define BOOT_NET_OSOTB_LOG_ENABLED				1		/*<! Enables module log. */
#define	LOG_TAG						"boot_net_osotb"	/*<! Tag assigned to logs for this module. */

#if BOOT_NET_OSOTB_LOG_ENABLED
	#define BOOT_NET_OSOTB_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_NET_OSOTB_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_NET_OSOTB_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define BOOT_NET_OSOTB_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define BOOT_NET_OSOTB_LOG_ERROR(...)
	#define BOOT_NET_OSOTB_LOG_WARNING(...)
	#define BOOT_NET_OSOTB_LOG_INFO(...)
	#define BOOT_NET_OSOTB_LOG_DEBUG(...)
#endif

// Private functions declaration ----------------------------------------------
ret_code_t boot_net_osotb_fsm_sv_work_ongoing_handler(void *arg);
ret_code_t boot_net_osotb_fsm_sv_create_ongoing_handler(void *arg);
ret_code_t boot_net_osotb_fsm_sv_send_ongoing_handler(void *arg);

/* Commands rx handlers: */
ret_code_t boot_net_osotb_rx_handler_get_uptime(char *payload, char *payload_response);
ret_code_t boot_net_osotb_rx_handler_get_def_app_slot(char *payload, char *payload_response);
ret_code_t boot_net_osotb_rx_handler_put_def_app_slot(char *payload, char *payload_response);

// Private variables declaration ----------------------------------------------
/* States of the fsm of this module: */
api_fsm_state_t boot_net_osotb_fsm_states[] =
{
	API_FSM_DEFINE_STATE(	BOOT_NET_FSM_SV_DISCONNECTED,
							NULL,
							NULL,
							NULL),
	API_FSM_DEFINE_STATE(	BOOT_NET_FSM_SV_CREATE,
							NULL,
							boot_net_osotb_fsm_sv_create_ongoing_handler,
							NULL),
	API_FSM_DEFINE_STATE(	BOOT_NET_FSM_SV_WORK,
							NULL,
							boot_net_osotb_fsm_sv_work_ongoing_handler,
							NULL),
	API_FSM_DEFINE_STATE(	BOOT_NET_FSM_SV_SEND,
							NULL,
							boot_net_osotb_fsm_sv_send_ongoing_handler,
							NULL),
};

/* Transitions of the fsm of this module: */
api_fsm_transition_t boot_net_osotb_fsm_transitions_map[] =
{
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_DISCONNECTED, BOOT_NET_EV_SV_CONNECTED, BOOT_NET_FSM_SV_CREATE),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_CREATE, BOOT_NET_EV_SV_CREATED, BOOT_NET_FSM_SV_WORK),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_WORK, BOOT_NET_EV_SV_SEND_DATA_REQUEST, BOOT_NET_FSM_SV_SEND),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_SEND, BOOT_NET_EV_SV_DATA_TX_SUCCESS, BOOT_NET_FSM_SV_WORK),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_SEND, BOOT_NET_EV_SV_DATA_TX_TIMEOUT, BOOT_NET_FSM_SV_WORK),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_SEND, BOOT_NET_EV_SV_DISCONNECTED, BOOT_NET_FSM_SV_DISCONNECTED),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_WORK, BOOT_NET_EV_SV_DISCONNECTED, BOOT_NET_FSM_SV_DISCONNECTED),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(BOOT_NET_FSM_SV_CREATE, BOOT_NET_EV_SV_DISCONNECTED, BOOT_NET_FSM_SV_DISCONNECTED),
};

// Private variables definition -----------------------------------------------
api_osotb_rx_handler_table_item_t rx_table[] =
{
	API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(API_OSOTB_METHOD_GET, "uptime", boot_net_osotb_rx_handler_get_uptime),
	API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(API_OSOTB_METHOD_GET, "def_app_slot", boot_net_osotb_rx_handler_get_def_app_slot),
	API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(API_OSOTB_METHOD_PUT, "def_app_slot", boot_net_osotb_rx_handler_put_def_app_slot)
};

#define OSOTB_RX_SIZEOF_TABLE (sizeof(rx_table) / sizeof(rx_table[0]))

api_osotb_rx_t api_osotb_rx =
{
	.table = rx_table,
	.table_size = OSOTB_RX_SIZEOF_TABLE,
};

// Public functions definition  -----------------------------------------------
/*
 * @brief initalize the ftp server task.
 * @return RET_OK if success.
 * */
ret_code_t boot_net_osotb_init(void)
{
	ret_code_t ret = RET_ERR;
	memset(&boot_net_osotb, 0, sizeof(boot_net_osotb));
	boot_net_osotb.sv_id = BOOT_NET_OSOTB_SERVER_ID;

	boot_net_osotb.fsm.actual_state = &boot_net_osotb_fsm_states[0];
	boot_net_osotb.fsm.previous_state = &boot_net_osotb_fsm_states[0];
	boot_net_osotb.fsm.state_table = boot_net_osotb_fsm_states;
	boot_net_osotb.fsm.transition_map = boot_net_osotb_fsm_transitions_map;
	boot_net_osotb.fsm.states_qty = BOOT_NET_FSM_MAX_STATES;
	boot_net_osotb.fsm.transitions_qty = BOOT_NET_MAX_EVENTS;

 	boot_net_osotb.task.cmd_queue = xQueueCreate(BOOT_NET_OSOTB_QUEUE_LEN, sizeof(boot_net_msg_t));
	if( NULL != boot_net_osotb.task.cmd_queue )
	{
		// TODO check if the queue was created
		if( xTaskCreate(	boot_net_task_template, "boot_net_osotb",
							BOOT_NET_OSOTB_TASK_STACK_SIZE,
							&boot_net_osotb,	/*!< Pass a pointer to the server struct to the task function to handle the server fsm. */
							BOOT_NET_OSOTB_TASK_PRIOTY,
							&boot_net_osotb.task.handle ) != pdPASS )
		{
			/* If the task can't be created the task handle will be NULL. */
			boot_net_osotb.task.handle = NULL;
		}
		else
		{
			ret = RET_OK;
		}
	}
	return ret;
}

// Private functions definition -----------------------------------------------
/* Task Message handlers ----------------------------------------------------*/
/**
 * @brief  process BOOT_NET_MSG_NET_DOWN_EVENT message.
 * @param msg queue message
 */
void boot_net_osotb_process_msg_net_down_event(boot_net_msg_t *msg)
{
	BOOT_NET_OSOTB_LOG_WARNING("Device disconnected from the network.");
	/* Trigger the event that says to the FSM that the device is disconnected
	 * from the network. */
	api_fsm_process_event(&boot_net_osotb.fsm, BOOT_NET_EV_SV_DISCONNECTED, NULL);
}

/**
 * @brief  process BOOT_NET_MSG_NET_UP_EVENT message.
 * @param msg queue message
 */
void boot_net_osotb_process_msg_net_up_event(boot_net_msg_t *msg)
{
	api_fsm_process_event(&boot_net_osotb.fsm, BOOT_NET_EV_SV_CONNECTED, NULL);
}

/**
 * @brief process a query of the status of the server.
 */
void boot_net_osotb_process_msg_query_sv_status(boot_net_msg_t *msg)
{
	if( NULL != msg )
	{
		if( (BOOT_NET_FSM_SV_WORK == boot_net_osotb.fsm.actual_state->id) ||
			(BOOT_NET_FSM_SV_SEND == boot_net_osotb.fsm.actual_state->id) )
		{
			boot_app_msg_sv_status_t sv_status = {0};
			sv_status.sv_id = BOOT_NET_OSOTB_SERVER_ID;
			sv_status.sv_up = true;
			boot_app_send_msg_sv_status(sv_status);
		}
	}
	else
	{
		BOOT_NET_OSOTB_LOG_ERROR("%s: msg is null!", __func__);
	}
}

/* FSM on_entry, ongoing and on_exit handlers -------------------------------*/
/*
 * @brief in this state, the task tries to create the server continuosly. And if
 * the server is created sucessfully, then go to the next state.
 * */
ret_code_t boot_net_osotb_fsm_sv_create_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	ret = api_osotb_server_create(&api_osotb_rx);
	if( RET_OK == ret )
	{
		BOOT_NET_OSOTB_LOG_INFO("%s: OSOTB server created!", __func__);
		api_fsm_process_event(&boot_net_osotb.fsm, BOOT_NET_EV_SV_CREATED, NULL);
	}
	return ret;
}

/*
 * @brief do an osotb server working cycle.
 * */
ret_code_t boot_net_osotb_fsm_sv_work_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	ret = api_osotb_server_work();
	if( RET_OK == ret )
	{

	}
	return ret;
}

/*
 * @brief N/A
 * TODO this must be implemented.
 * */
ret_code_t boot_net_osotb_fsm_sv_send_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	// TODO implement this
	return ret;
}

/* RX handlers of the OSOTB server --------------------------------------------*/
/**
 * @brief handler executed when a GET to the UPTIME resource is received.
 * @param payload in this case, the payload of the request is unused.
 * @param payload_response in this case, in the payload_response will be stored the
 * uptime of the devide.
 * @return RET_OK if success.
 */
ret_code_t boot_net_osotb_rx_handler_get_uptime(char *payload, char *payload_response)
{
	if( NULL == payload_response ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	(void) payload;
	char timestamp_str[API_DATETIME_TS_STR_LEN];
	API_Datetime_Get_Timestamp(timestamp_str, sizeof(API_DATETIME_TS_STR_LEN));
	sprintf(payload_response, "%s(%lu)", timestamp_str, TICKS_TO_MS(xTaskGetTickCount()));
	return ret;
}

/**
 * @brief handler executed when a GET to the def_app_slot resource is received.
 * @param payload in this case, the payload of the request is unused.
 * @param payload_response in this case, in the payload_response will store the
 * default app_slot of the device.
 * @return RET_OK if success.
 */
ret_code_t boot_net_osotb_rx_handler_get_def_app_slot(char *payload, char *payload_response)
{
	if( NULL == payload_response ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	(void) payload;
	api_iap_app_slot_t def_app_slot = api_iap_get_default_app();
	sprintf(payload_response, "%lu", (uint32_t) def_app_slot);
	return ret;
}

/**
 * @brief handler executed when a PUT to the def_app_slot resource is received.
 * @param payload in this case, the payload of request will have the default app slot to set.
 * @param payload_response in this case, returns the value flashed.
 * @return RET_OK if success.
 */
ret_code_t boot_net_osotb_rx_handler_put_def_app_slot(char *payload, char *payload_response)
{
	if( NULL == payload_response ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;

	api_iap_app_slot_t app_slot = (api_iap_app_slot_t) atoi(payload);
	ret = api_iap_set_default_app(app_slot);
	if( RET_OK != ret )
	{
		BOOT_NET_OSOTB_LOG_ERROR("%s: app_slot number not recognized! (app_slot %d)", __func__, app_slot);
	}
	else
	{
		BOOT_NET_OSOTB_LOG_INFO("%s: app_slot flashed correctly! (app_slot %d)", __func__, app_slot);
	}

	// put in the payload_response the value burned in flash
	sprintf(payload_response, "%d", api_iap_get_default_app());

	return ret;
}
